-- Discipline Priest for 8.1 by sboc - 10/2018
-- Talents: All supported
-- Holding Alt = Power word barrier (ground)
-- Holding Shift = MindBender / Shadowfiend
-- Holding Control = Massdispell
local race = UnitRace("player")
local realmName = GetRealmName()
local addon, dark_addon = ...
local SB = dark_addon.rotation.spellbooks.priest

local function combat()

  local group_health_percent = 100 * UnitHealth("player") / UnitHealthMax("player") or 0
  local group_health = group_health_percent
  local group_unit_count = IsInGroup() and GetNumGroupMembers() or 1
  local damaged_units = group_health_percent < 90 and 1 or 0
  local dead_units = 0
  local units = {}
  for i = 1,group_unit_count-1 do
    local name, rank, subgroup, level, class, fileName, zone, online, isDead, role, isML = GetRaidRosterInfo(i);
    local unit = IsInRaid() and "raid"..i or "party"..i
    local unit_health = 100 * UnitHealth(unit) / UnitHealthMax(unit) or 0
    if unit_health < 90 then
      damaged_units = damaged_units + 1
      table.insert(units, 1, {unit, unit_health})
    end
    if isDead or not online or not UnitInRange(unit) then
      dead_units = dead_units + 1
    else
      group_health = group_health + unit_health
    end
  end
  group_health_percent = group_health / (group_unit_count - dead_units)
  table.sort(units, function(a, b) return a[2] < b[2] end)

  -- Shift = find/bender
  if modifier.shift and talent(3, 2) and -spell(SB.Mindbender) == 0 and target and target.enemy then
    return cast(SB.Mindbender, target)
  elseif modifier.shift and not talent(6, 3) and -spell(SB.Shadowfiend) == 0 and target and target.enemy then
    return cast(SB.Shadowfiend)
  end

  -- Alt barrier
  if modifier.alt and not talent(7,2) and -spell(SB.PowerWordBarrier) == 0 then
    return cast(SB.PowerWordBarrier, 'ground')
  elseif modifier.alt and talent(7,2) and -spell(SB.LuminousBarrier) == 0 then
    return cast(SB.LuminousBarrier)
  end

  -- Ctrl MD
  if modifier.control and -spell(SB.MassDispell) == 0 then
    return cast(SB.MassDispell, 'ground')
  end

  -- Main rotation
  if player.alive and not player.channeling() then -- target.alive and target.enemy then

    --Dispel
    local dispellable_unit = group.removable('disease', 'magic')
    if toggle('dispel', false) and dispellable_unit and spell(SB.Purify).cooldown == 0 then
      return cast(SB.Purify, dispellable_unit)
    end

    -- CD tanks
    if toggle('tankheal', false) then
      if tank.health.percent < 20 and toggle('cooldowns', false) and -spell(SB.PainSuppression) == 0 then
        return cast(SB.PainSuppression, tank)
      end

      if offtank.health.percent < 20 and toggle('cooldowns', false) and -spell(SB.PainSuppression) == 0 then
        return cast(SB.PainSuppression, offtank)
      end
    end

    -- CD
    if lowest.health.percent < 30 and toggle('cooldowns', false) and -spell(SB.PainSuppression) == 0 then
      return cast(SB.PainSuppression, lowest)
    end

    -- Check if player is low
    if player.health.percent < 30 and toggle('cooldowns', false) and -spell(SB.DesperatePrayer) == 0 then
      return cast(SB.DesperatePrayer)
    end

    -- Check if tank is dropping low
    if toggle('tankheal', false) and not player.moving then
      if tank.health.percent < 50 and -spell(SB.ShadowMend) == 0 then
        return cast(SB.ShadowMend, tank)
      end

      if offtank.health.percent < 50 and -spell(SB.ShadowMend) == 0 then
        return cast(SB.ShadowMend, offtank)
      end
    end

    -- Check if one is low
    if not player.moving and lowest.health.percent < 50 and -spell(SB.ShadowMend) == 0 then
      return cast(SB.ShadowMend, lowest)
    end

    -- Check if one is low
    if  toggle('tankheal', false) and player.moving then
      if tank.health.percent < 70 and -spell(SB.Penance) == 0 then
        return cast(SB.Penance, tank)
      end

      if offtank.health.percent < 70 and -spell(SB.Penance) == 0 then
        return cast(SB.Penance, offtank)
      end
    end

    -- Check if one is low
    if player.moving and lowest.health.percent < 70 and -spell(SB.Penance) == 0 then
      return cast(SB.Penance, lowest)
    end

    -- Radiance
    if damaged_units >= 3 and not player.moving and -spell(SB.PowerWordRadiance) == 0 and tank.buff(SB.Atonement).down then
      cast(SB.PowerWordRadiance, tank)
    end

    -- Radiance
    if damaged_units >= 3 and not player.moving and -spell(SB.PowerWordRadiance) == 0 and not player.spell(SB.PowerWordRadiance).lastcast and lowest.buff(SB.Atonement).down then
      cast(SB.PowerWordRadiance, lowest)
    end

    -- Apply Atonement to tank
    if tank.health.percent < 95 and -spell(SB.PowerWordShield) == 0 and tank.buff(SB.Atonement).remains < 2 then
      return cast(SB.PowerWordShield, tank)
    end

    -- Apply Atonement to tank
    if offtank.health.percent < 95 and -spell(SB.PowerWordShield) == 0 and offtank.buff(SB.Atonement).remains < 2 then
      return cast(SB.PowerWordShield, offtank)
    end

    -- Apply Atonement to lowest
    if lowest.health.percent < 95 and -spell(SB.PowerWordShield) == 0 and lowest.buff(SB.Atonement).remains < 2 then
      return cast(SB.PowerWordShield, lowest)
    end

    if target and target.enemy then

      -- SW:P
      if not talent(6,1) and target.debuff(SB.ShadowWordPain).remains < 3 then
        return cast(SB.ShadowWordPain, 'target')
      end

      -- PtW
      if talent(6,1) and target.debuff(SB.PurgetheWicked).remains < 3 then
        return cast(SB.PurgetheWicked, 'target')
      end

      -- Penance
      if (player.moving or -buff(SB.PowerOfTheDarkSide)) and -spell(SB.Penance) == 0 then
        return cast(SB.Penance, target)
      end

      -- Penance spread purge
      if talent(6,1) and -spell(SB.Penance) == 0 and target.debuff(SB.PurgetheWicked).remains > 2 then
        return cast(SB.Penance, 'target')
      end

      -- Schism
      if not player.moving and -spell(SB.Schism) == 0 and talent(1,3) then
        return cast(SB.Schism, target)
      end

      -- Schism
      if -spell(SB.PowerWordSolace) == 0 and talent(3,3) then
        return cast(SB.PowerWordSolace, target)
      end

      -- Smite
      if not player.moving and -spell(SB.Smite) == 0 then
        return cast(SB.Smite, target)
      end
    end

  end

end

-- Put great stuff here to do when your out of combat
local function resting()


  --Dispel
  local dispellable_unit = group.removable('disease', 'magic')
  if toggle('dispel', false) and dispellable_unit and spell(SB.Purify).cooldown == 0 then
    return cast(SB.Purify, dispellable_unit)
  end


  -- Check if one is low
  if not player.moving and lowest.health.percent < 70 and -spell(SB.ShadowMend) == 0 then
    return cast(SB.ShadowMend, lowest)
  end

  -- Check if one is low
  if lowest.health.percent < 70 and -spell(SB.Penance) == 0 then
    return cast(SB.Penance, lowest)
  end
  
  -- Smite
  if target and not player.moving and target.enemy and -spell(SB.Smite) == 0 then
    return cast(SB.Smite, target)
  end
end

function interface()
  dark_addon.interface.buttons.add_toggle({
    name = 'tankheal',
    label = 'Tank Healing',
    on = {
      label = 'TANK',
      color = dark_addon.interface.color.orange,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'tank',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })

  dark_addon.interface.buttons.add_toggle({
    name = 'dispel',
    label = 'Auto Dispel',
    on = {
      label = 'DSP',
      color = dark_addon.interface.color.red,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.red, 0.7)
    },
    off = {
      label = 'dsp',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
end

dark_addon.rotation.register({
  spec = dark_addon.rotation.classes.priest.discipline,
  name = 'discipline',
  label = 'Disc by SBOC',
  combat = combat,
  resting = resting,
  interface = interface
})
