-- Frost Mage for 8.1 by Rotations - 8/2018
-- Talents: 2 2 1 3 2 2 1 (supports any talents but these are the NON glacial spike talents)
-- Holding Alt = IceBarrier
-- Holding Shift = Blizzard
local race = UnitRace("player")
local realmName = GetRealmName()
local addon, dark_addon = ...
local SB = dark_addon.rotation.spellbooks.mage

local function combat()
  if target.alive and target.enemy and player.alive then
    -- Let's read what the GUI has to say
    local blowup = dark_addon.settings.fetch('dr_example_blowup')
    local intpercent = dark_addon.settings.fetch('dr_example_spinner')
    local usehealthstone = dark_addon.settings.fetch('dr_example_healthstone.check')
    local healthstonepercent = dark_addon.settings.fetch('dr_example_healthstone.spin')
    local callpet = dark_addon.settings.fetch('dr_example_callpet')
    local usepoly = dark_addon.settings.fetch('dr_example_usepolymorph')
    local usefrostnova = dark_addon.settings.fetch('dr_example_usefrostnova')
    local heavymove = dark_addon.settings.fetch('dr_example_heavymovement')
    local usespellsteal = dark_addon.settings.fetch('dr_example_usespellsteal')
    local nameplates = dark_addon.settings.fetch('dr_example_usenameplates')
    local critChance = GetCritChance() -- in development for future use

    local inRange = 0
    for i = 1, 40 do
      if UnitExists('nameplate' .. i) and IsSpellInRange('Ebonbolt', 'nameplate' .. i) == 1 and UnitAffectingCombat('nameplate' .. i) then 
        inRange = inRange + 1
      end 
    end

    -- heavymovement logic from GUI
    if heavymove == true and player.moving then
      if -buff(SB.BrainFreeze) and not player.spell(SB.Flurry).lastcast then
        return cast(SB.Flurry)
      end

      if -buff(SB.FingersofFrost) then
        return cast(SB.IceLance)
      end

      if modifier.shift and -spell(SB.ConeofCold) == 0 then 
        return cast(SB.ConeofCold)
      end

      if modifier.shift and -spell(SB.FrozenOrb) == 0 then
        return cast(SB.FrozenOrb)
      end

      if target.has_stealable and usespellsteal == true and castable(SB.SpellSteal, 'target') then
        return cast(SB.SpellSteal, 'target')
      end
    end

    if not -buff(SB.IceBarrior) and player.moving  and toggle('ice_barrier', false) and player.alive and -spell(SB.IceBarrior) == 0 then
      print '62'
      return cast(SB.IceBarrior, 'player')
    end

    if not player.moving and player.spell(SB.GlacialSpike).lastcast and -buff(SB.BrainFreeze) and not player.spell(SB.Flurry).lastcast then
      return cast(SB.Flurry)
    end

    if not player.moving and player.spell(SB.Flurry).lastcast then
      return cast(SB.IceLance, 'target')
    end

    if not player.moving and  talent(7,3) and castable(SB.GlacialSpike, 'target') and -buff(SB.BrainFreeze) then 
      return cast(SB.GlacialSpike)
    end

    if -buff(SB.BrainFreeze) and player.spell(SB.Frostbolt).lastcast and buff(SB.Icicles).count <= 2 then
      return cast(SB.Flurry, 'target')
    end

    if not player.moving and  -buff(SB.BrainFreeze) then 
      return cast(SB.Frostbolt)
    end

    if castable(SB.Counterspell, 'target') and target.interrupt(intpercent, false) then
      return cast(SB.Counterspell, 'target')
    end

    if not player.moving and  -buff(SB.WintersReach) and -spell(SB.Flurry) == 0 and not player.spell(SB.Flurry).lastcast then
      return cast(SB.Flurry)
    end

    if not player.moving and  -buff(SB.WintersReachOther) and -spell(SB.Flurry) == 0 and not player.spell(SB.Flurry).lastcast then
      return cast(SB.Flurry)
    end

    if modifier.alt then
      return cast(SB.IceBarrior)
    end
    
    if modifier.shift and not player.moving then
      return cast(SB.Blizzard, 'ground')
    end

    if not player.moving and talent(7,3) and castable(SB.GlacialSpike, 'target') and toggle('gspike', false) then 
      return cast(SB.GlacialSpike)
    end

    if talent(6,3) and -spell(SB.CometStorm) == 0 and toggle('cooldowns', false) then
      return cast(SB.CometStorm)
    end

    if not player.moving and castable(SB.Ebonbolt, 'target')  and toggle('cooldowns', false) then
      return cast(SB.Ebonbolt, 'target')
    end

    if castable(SB.IcyVeins) and toggle('cooldowns', false) then
      return cast(SB.IcyVeins, 'target')
    end

    if castable(SB.FrozenOrb) and toggle('multitarget', false) then
      return cast(SB.FrozenOrb, 'target')
    end

    if buff(SB.FingersofFrost).count > 0 then
      return cast(SB.IceLance)
    end

    if GetItemCooldown(5512) == 0 and player.health.percent < healthstonepercent and usehealthstone == true then
      macro('/use Healthstone')
    end

    if usefrostnova == true and not -target.debuff(SB.FrostNova) and spell(SB.FrostNova).charges >= 1 and target.distance <= 12 then
      return cast(SB.FrostNova)
    end

    if usepoly == true and not -target.debuff(SB.Polymorph) and not player.spell(SB.Polymorph).lastcast then
      return cast(SB.Polymorph, 'target')
    end

    if target.has_stealable and usespellsteal == true and castable(SB.SpellSteal, 'target') then
      return cast(SB.SpellSteal, 'target')
    end

    if player.moving then
      return cast(SB.IceLance)
    end

    -- Filler below
    return cast(SB.Frostbolt, 'target')
  end
end

-- Put great stuff here to do when your out of combat
local function resting()
  local nameplates = dark_addon.settings.fetch('dr_example_usenameplates', false)
  local seeplates = GetCVar("nameplateShowEnemies")
  local callpet = dark_addon.settings.fetch('dr_example_callpet')

  if nameplates == true and seeplates == '0' then
     SetCVar("nameplateShowEnemies",1)
  end
  if nameplates == false and seeplates == '1' then
     SetCVar("nameplateShowEnemies",0)
  end

  if not pet.exists and callpet == true and player.alive then 
    return cast(SB.SummonWaterElemental)
  end

  if not -buff(SB.IceBarrior) and player.moving  and toggle('ice_barrier', false) and player.alive then
    return cast(SB.IceBarrior, 'player')
  end
end

local function interface()
  local example = {
    key = 'dr_example',
    title = 'DarkRotations - Mage',
    width = 250,
    height = 320,
    resize = true,
    show = false,
    template = {
      { type = 'header', text = '               Frost Mage Settings' },
      { type = 'text', text = 'Everything on the screen is LIVE.  As you make changes, they are being fed to the engine.' },
      { type = 'rule' },   
      { type = 'text', text = 'General Settings' },
      { key = 'callpet', type = 'checkbox', text = 'Call Pet', desc = 'Always call Water Elemental ' },
      { key = 'usenameplates', type = 'checkbox', text = 'Show Enemy Nameplates', desc = 'Use name plates to count baddies' },
      { key = 'healthstone', type = 'checkspin', text = 'Healthstone', desc = 'Auto use Healthstone at health %', min = 5, max = 100, step = 5 },
      -- { key = 'input', type = 'input', text = 'TextBox', desc = 'Description of Textbox' },
      { key = 'spinner', type = 'spinner', text = 'Interupt %', desc = 'What % you will be interupting at', min = 5, max = 100, step = 5 },
      { type = 'rule' },   
      { type = 'text', text = 'PVP Options' },
      { key = 'usepolymorph', type = 'checkbox', text = 'Polymorph', desc = 'Polymorph your target if possible' },
      { key = 'usefrostnova', type = 'checkbox', text = 'Frost Nova', desc = 'Auto use Frost Nova if target is near' },
      { key = 'usespellsteal', type = 'checkbox', text = 'Spell Steal', desc = 'Use Spell Steal when possible' },
      { type = 'rule' },   
      { type = 'text', text = 'Raid / M+ / Party Options' },
      { key = 'heavymovement', type = 'checkbox', text = 'Heavy movement fight', desc = 'Rotation will be based on heavy movement' },
    }
  }

  configWindow = dark_addon.interface.builder.buildGUI(example)

  dark_addon.interface.buttons.add_toggle({
    name = 'ice_barrier',
    label = 'Ice Barrier Auto Cast',
    font = 'dark_addon_icon',
    on = {
      label = dark_addon.interface.icon('globe'),
      color = dark_addon.interface.color.orange,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.dark_orange, 0.7)
    },
    off = {
      label = dark_addon.interface.icon('globe'),
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })

  dark_addon.interface.buttons.add_toggle({
     name = 'gspike',
    label = 'Use GlacialSpike on CD (AOE)',
    font = 'dark_addon_icon',
    on = {
      label = dark_addon.interface.icon('cube'),
      color = dark_addon.interface.color.blue,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.purple, 0.7)
    },
    off = {
      label = dark_addon.interface.icon('cube'),
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })

  dark_addon.interface.buttons.add_toggle({
    name = 'settings',
    label = 'Rotation Settings',
    font = 'dark_addon_icon',
    on = {
      label = dark_addon.interface.icon('cog'),
      color = dark_addon.interface.color.cyan,
      color2 = dark_addon.interface.color.dark_cyan
    },
    off = {
      label = dark_addon.interface.icon('cog'),
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    },
    callback = function(self)
      if configWindow.parent:IsShown() then
        configWindow.parent:Hide()
      else
        configWindow.parent:Show()
      end
    end
  })
end

dark_addon.rotation.register({
  spec = dark_addon.rotation.classes.mage.frost,
  name = 'frost',
  label = 'Bundled Frost',
  combat = combat,
  resting = resting,
  interface = interface
})
