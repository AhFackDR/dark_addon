-- Firemage for 8.1 by Rotations - 8/2018
-- Talents: 2 2 1 3 2 2 1
-- Holding Alt = DragonsBreath
-- Holding Shift = Flamestrike

local addon, dark_rotation = ...
local SB = dark_rotation.rotation.spellbooks.mage

local function combat()

  if target.alive and target.enemy and player.alive then
     -- added
    if castable(SB.Counterspell, 'target') and target.interrupt(100, false) then
      return cast(SB.Counterspell, 'target')
    end

	 if toggle('polymorph', false) and not -target.debuff(SB.Polymorph) then
		return cast(SB.Polymorph, 'target')
		end

	if modifier.alt and -spell(SB.DragonsBreath) == 0 then
      return cast(SB.DragonsBreath, 'target')
    end
    if modifier.shift and -spell(SB.Flamestrike) == 0 and -buff(SB.HotStreak) then
      return cast(SB.Flamestrike, 'ground')
    end

    if -buff(SB.HotStreak) then
      return cast(SB.Pyroblast, 'target')
    end
    if -buff(SB.HeatingUp) and spell(SB.FireBlast).charges > 0 and toggle('phoenix_flames', false) then
      return cast(SB.FireBlast)
	  end
    if -buff(SB.HeatingUp) and spell(SB.PhoenixFlames).charges > 0 and toggle('phoenix_flames', false) then
      return cast(SB.PhoenixFlames)
	  end

    if player.moving  then
      return cast(SB.Scorch, 'target')
    end
    if -spell(SB.BlazingBarrier) == 0 and not -buff(SB.BlazingBarrier) and toggle('blazing_barrier', false) then
      return cast(SB.BlazingBarrier, 'player')
    end
    if -spell(SB.Combustion) == 0 and toggle('cooldowns', false) then
      return cast(SB.Combustion, 'player')
    end

    if -buff(SB.Combustion) then
	    return cast(SB.Scorch)
	   end


    return cast(SB.Fireball, 'target')
    end
end

local function resting()
	if toggle('blazing_barrier', false) and -spell(SB.BlazingBarrier) == 0 and not -buff(SB.BlazingBarrier) and player.moving then
    return cast(SB.BlazingBarrier, 'player')
  end
  -- Put great stuff here to do when your out of combat
end

local function interface()
   dark_rotation.interface.buttons.add_toggle({
    name = 'blazing_barrier',
    on = {
      label = 'BB',
      color = dark_rotation.interface.color.orange,
      color2 = dark_rotation.interface.color.ratio(dark_rotation.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'BB',
      color = dark_rotation.interface.color.grey,
      color2 = dark_rotation.interface.color.dark_grey
    }
  })

  dark_rotation.interface.buttons.add_toggle({
    name = 'phoenix_flames',
    on = {
      label = 'PF',
      color = dark_rotation.interface.color.red,
      color2 = dark_rotation.interface.color.ratio(dark_rotation.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'PF',
      color = dark_rotation.interface.color.grey,
      color2 = dark_rotation.interface.color.dark_grey
    }
  })

  dark_rotation.interface.buttons.add_toggle({
    name = 'polymorph',
    on = {
      label = 'PM',
      color = dark_rotation.interface.color.green,
      color2 = dark_rotation.interface.color.ratio(dark_rotation.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'PM',
      color = dark_rotation.interface.color.grey,
      color2 = dark_rotation.interface.color.dark_grey
    }
  })
end

dark_rotation.rotation.register({
  spec = dark_rotation.rotation.classes.mage.fire,
  name = 'fire',
  label = 'Bundled Fire',
  combat = combat,
  resting = resting,
  interface = interface
})

--{ "Pyroblast", { "player.buff(Kael'thas's Ultimate Ability)", "!player.buff(48108)", "!modifier.last(Pyroblast)", "!player.moving"}},
-- "player.spell(Fire Blast).charges < 1"
